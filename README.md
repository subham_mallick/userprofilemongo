# User profile service POC with mongo

Please open the ```/userprofile-sprintboot-mongo-redis.pdf``` file for the project overview.

## Prerequisites
* Docker
* Redis

## RUN

* Load the project in Intellij 
* Start the docker locally 
* Run the docker-compose.yaml file to run the
  * ```mongo``` on port ```27017```
  * ```mongoexpress``` on port ```8081```
* Start the Redis server on port ``6379``
* Start the Spring boot app