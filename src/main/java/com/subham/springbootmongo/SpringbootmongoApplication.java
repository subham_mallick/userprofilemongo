package com.subham.springbootmongo;

import com.subham.springbootmongo.model.*;
import com.subham.springbootmongo.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.time.LocalDateTime;

@SpringBootApplication
@EnableCaching
public class SpringbootmongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootmongoApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(UserRepository userRepository, MongoTemplate mongoTemplate) {

        return args -> {
            String email1 = "subham1mallick@gmail.com";
            String email2 = "subham2mallick@gmail.com";
            String email3 = "subham3mallick@gmail.com";
            String email4 = "subham4mallick@gmail.com";
            String email5 = "subham5mallick@gmail.com";

            insertToMongo(userRepository, email1, createUser(email1));
            insertToMongo(userRepository, email2, createUser(email2));
            insertToMongo(userRepository, email3, createUser(email3));
            insertToMongo(userRepository, email4, createUser(email4));
            insertToMongo(userRepository, email5, createUser(email5));
        };
    }

    private User createUser(String email) {
        return new User(
                "Subham",
                "Mallick",
                email,
                Gender.MALE,
                AddressBuilder.anAddress().withCity("Barasat").withCountry("India").withPostCode("700126").build(),
                Status.ACTIVE,
                "9999999999",
                null,
                LocalDateTime.now(),
                LocalDateTime.now()
        );
    }

    private void insertToMongo(UserRepository userRepository, String email, User user) {
        userRepository.findUserByEmail(email)
                .ifPresentOrElse(
                        i_user -> System.out.println("Already exists " + i_user),
                        () -> {
                            System.out.println("Inserting User " + user);
                            userRepository.insert(user);
                        }
                );
    }
}
