package com.subham.springbootmongo.model;

public enum Gender {
    MALE,
    FEMALE,
    OTHERS
}
