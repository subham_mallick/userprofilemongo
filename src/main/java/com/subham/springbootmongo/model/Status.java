package com.subham.springbootmongo.model;

public enum Status {
    ACTIVE,
    INACTIVE,
    BLACKLISTED
}
