package com.subham.springbootmongo.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class Address implements Serializable {

    private String country;
    private String city;
    private String postCode;
}
