package com.subham.springbootmongo.model;

public final class AddressBuilder {
    private String country;
    private String city;
    private String postCode;

    private AddressBuilder() {
    }

    public static AddressBuilder anAddress() {
        return new AddressBuilder();
    }

    public AddressBuilder withCountry(String country) {
        this.country = country;
        return this;
    }

    public AddressBuilder withCity(String city) {
        this.city = city;
        return this;
    }

    public AddressBuilder withPostCode(String postCode) {
        this.postCode = postCode;
        return this;
    }

    public Address build() {
        return new Address(country, city, postCode);
    }
}
