package com.subham.springbootmongo.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Document
public class User implements Serializable {
    @Id
    private String id;
    private String firstname;
    private String lastname;
    @Indexed(unique = true)
    private String email;
    private Gender gender;
    private Address address;
    private Status status;
    private String phoneNumberMain;
    private String phoneNumberAltr;
    private LocalDateTime dateOfBirth;
    private LocalDateTime dateOfRegistration;

    public User(String firstname,
                String lastname,
                String email,
                Gender gender,
                Address address,
                Status status,
                String phoneNumberMain,
                String phoneNumberAltr,
                LocalDateTime dateOfBirth,
                LocalDateTime dateOfRegistration) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.gender = gender;
        this.address = address;
        this.status = status;
        this.phoneNumberMain = phoneNumberMain;
        this.phoneNumberAltr = phoneNumberAltr;
        this.dateOfBirth = dateOfBirth;
        this.dateOfRegistration = dateOfRegistration;
    }
}
