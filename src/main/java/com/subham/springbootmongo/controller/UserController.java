package com.subham.springbootmongo.controller;

import com.subham.springbootmongo.exception.ResourceNotFoundException;
import com.subham.springbootmongo.model.User;
import com.subham.springbootmongo.service.UserService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1")
@AllArgsConstructor
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;


    @GetMapping("/users")
    @Cacheable(value = "users")
    public List<User> getAllUsers() {
        logger.warn("HIT >> GET /users/");
        return userService.findAllUsers();
    }

    @GetMapping("/user/{email}")
    @Cacheable(value = "user", key = "#email")
    public User getUserByEmail(@PathVariable String email) throws ResourceNotFoundException {
        logger.warn("HIT >> GET /user/" + email);
        return userService.findUserByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this email :" + email));
    }

    @PostMapping("/user/add")
    public User createUser(@RequestBody User user) {
        logger.warn("HIT >> POST /user/add");
        return userService.saveUser(user);
    }

    @PutMapping("/user/{email}")
    @CachePut(value = "user", key = "#newUser.email")
    public User updateUser(@PathVariable String email, @RequestBody User newUser) throws ResourceNotFoundException {
        logger.warn("HIT >> PUT /user/" + email);
        User user = userService.findUserByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this email :" + email));

        user.setAddress(newUser.getAddress());
        user.setDateOfBirth(newUser.getDateOfBirth());
        user.setEmail(newUser.getEmail());
        user.setFirstname(newUser.getFirstname());
        user.setGender(newUser.getGender());
        user.setDateOfRegistration(newUser.getDateOfRegistration());
        user.setLastname(newUser.getLastname());
        user.setPhoneNumberMain(newUser.getPhoneNumberMain());
        user.setPhoneNumberAltr(newUser.getPhoneNumberAltr());
        user.setStatus(newUser.getStatus());
        return userService.saveUser(user);
    }

    @DeleteMapping("/user/{email}")
    @CacheEvict(value = "user", key = "#email")
    public void deleteUser(@PathVariable String email) throws ResourceNotFoundException {
        logger.warn("HIT >> DEL /user/" + email);
        User user = userService.findUserByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this email :" + email));
        userService.deleteUserByEmail(email);
    }


}
